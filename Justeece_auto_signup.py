#By Samuel Paul Yila
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

local_driver_path = 'C:\\webdrivers\\chromedriver.exe'

url = 'https://dev2.justeece.com/'

path_link = Service(local_driver_path)
driver = webdriver.Chrome(service=path_link)

driver.get(url)

signup_button = driver.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[4]/div/a[2]')
signup_button.click()

first_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/div[1]/input')
last_name = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/div[3]/input')
email = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/div[5]/input')
password = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/div[6]/input')

accept_licence_check_box = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/div[8]/label')
create_my_account = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/form/button')

#fill in the new user details
first_name.send_keys('user_first_name')
last_name.send_keys('user_last_name')
email.send_keys('user_email_address')
password.send_keys('password_of_your_choice')
time.sleep(5)

#accept terms and condition by clicking on checkbox
driver.execute_script('arguments[0].scrollIntoView()', accept_licence_check_box)
accept_licence_check_box.click()
time.sleep(2)

#click create account
create_my_account.click()

time.sleep(5)
driver.quit()
